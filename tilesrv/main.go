package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"ocean-madness/tilesrv/github.com/consbio/mbtileserver/handlers"
)

var port = "8080"

func main() {
	if env := os.Getenv("PORT"); env != "" {
		_, err := strconv.Atoi(env)
		if err != nil {
			fmt.Fprintln(os.Stderr, "PORT must be a number")
			os.Exit(1)
		}
		port = env
	}

	svcSet := handlers.New()

	// quick HACK: I guess the server doesn't check XFH or something, and adding
	// an override here is less effort than detecting the proxy properly over
	// there
	if env := os.Getenv("DOMAIN"); env != "" {
		svcSet.Domain = env
	}

	// XXX not checking if valid args, cuz who cares
	for _, a := range os.Args[1:] {
		sp := strings.SplitN(a, ":", 2)
		if e := svcSet.AddDBOnPath(sp[1], sp[0]); e != nil {
			fmt.Fprintf(os.Stderr, "tilesrv: %v\n", e)
			os.Exit(200)
		}
	}

	e := echo.New()
	e.HideBanner = true
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	ef := func(err error) {
		fmt.Fprintf(os.Stderr, "tilesrv: %v\n", err)
	}
	h := echo.WrapHandler(svcSet.Handler(ef))
	e.GET("/*", h)

	listener, err := net.Listen("tcp", ":" + port)

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	server := &http.Server{Handler: e}
	fmt.Fprintln(os.Stderr, server.Serve(listener))
	os.Exit(200)
}

// vi: sw=4 ts=4 noet cc=80
