# for deploying, set BUILD_ENV=prod
BUILD_ENV := dev

AT := @
NODE := node
NPM := npm
TOUCH := touch
NULL := /dev/null
HINT_SRV := echo \
"Hint: run './make srv' in another terminal for a live-reloading test server"
# requires Unix and GNU Make - see GNUmakefile for implementation
# btw if you're wondering what's up with `:; : && :` I'll explain, it's very
# """funny:"""
# - :; is a no-op in sh and a comment in batch
# - just :; makes Windows GNU Make think it's a program and try to CreateProcess
#   directly for some insane reason, this obviously fails
# - the && is just there to break that heuristic so cmd /C is still used, and
#   of course it's just tacking on more comment/no-op stuff
# I'm crying.
PROGRESS_INFO = @:; : && :

RELOAD_CURL_dev = \
$(AT)curl http://localhost:8080/.reload/ 2>$(NULL) >$(NULL) || $(HINT_SRV)
RELOAD_CURL_prod := @:;
_: _all ; $(RELOAD_CURL_$(BUILD_ENV))

# XXX this should be generated but I'm low on time
TILE_FILES := \
	build/tiles/base.mbtiles \
	build/tiles/cities.mbtiles \
	build/tiles/munge2m.mbtiles

everything: _all build/tilesrv $(TILE_FILES); @:;

_eslint: build/.npmstamp
	$(PROGRESS_INFO) "LINT js/"
	$(AT)$(NODE) node_modules/eslint/bin/eslint.js -c lint/eslint.json js/*.js

_stylelint: build/.npmstamp
	$(PROGRESS_INFO) "LINT css/"
	$(AT) node_modules/stylelint/bin/stylelint.js --config lint/stylelint.json css/*.scss

lint: _eslint _stylelint

# this normally doesn't run, see build-config.json
TILESRV_DEP_true := build/host/tilesrv build/host/runall \
	build/tiles/base.mbtiles
TILESRV_CMD_true := build/host/runall \
	env PORT=8090 build/host/tilesrv \
		base:build/tiles/base.mbtiles \
		cities:build/tiles/cities.mbtiles "" \
	$(NODE) testsrv/ --proxy-tilesrv
TILESRV_CMD_false := $(NODE) testsrv/ \

include build/rules.mk

srv: build/.npmstamp $(TILESRV_DEP_$(TILESRV_ENABLE))
	$(AT)$(TILESRV_CMD_$(TILESRV_ENABLE))

# XXX this could be done in a nicer way huh...
build/tilesrv: _gobuild_ocean-madness__tilesrv
build/host/tilesrv: _gobuild_host_ocean-madness__tilesrv

.PHONY: _all _eslint _stylelint _htmlhint lint _ srv everything

build/int/lib.big.js: $(JS_FILES) scripts/mkjs build/.npmstamp
	$(PROGRESS_INFO) "MKJS"
	$(AT)$(NODE) scripts/mkjs $(JS_FILES)
-include build/int/js.dep

CP_PUB := cp pub/* build/pub

build/pub/lib.js: build/int/lib.big.js
	$(PROGRESS_INFO) "JS lib.js"
	$(AT)$(NODE) node_modules/uglify-es/bin/uglifyjs $(UGLYFLAGS) -o $@ $<
_all: build/pub/lib.js
#	XXX TODO should be deleting stale stuff but that's hard, for now it's manual
	$(AT)$(CP_PUB)

build/.npmstamp: package.json
#	loglevel to avoid advertising garbage
	$(AT)$(NPM) i --loglevel=warn && $(TOUCH) $@

CANOE_FLAGS := --coalesce-smallest-as-needed --detect-shared-borders \
	--extend-zooms-if-still-dropping -z8

build/tiles/base.mbtiles: gj/ashms-worldmap.geo.json build/host/tippecanoe
	$(PROGRESS_INFO) "TILES base"
	$(AT)build/host/tippecanoe --force $(CANOE_FLAGS) -o $@ $< || ( rm $@ && exit 1; )

build/tiles/cities.mbtiles: gj/drei01-cities.geo.json build/host/tippecanoe
	$(PROGRESS_INFO) "TILES cities"
	$(AT)build/host/tippecanoe --force $(CANOE_FLAGS) -o $@ $< || ( rm $@ && exit 1; )

build/tiles/munge2m.mbtiles: build/munge2m.geo.json build/host/tippecanoe
	$(PROGRESS_INFO) "TILES munge2m"
	$(AT)build/host/tippecanoe --force $(CANOE_FLAGS) -o $@ $< || ( rm $@ && exit 1; )

#build/munge2m.geo.json: munge

# TODO temp rule, this should have an output
# ALSO, should depend on all the .py files
munge: build/.pipstamp gj/earth-coastlines-1m.geo.json
	$(PROGRESS_INFO) "VALENMUNGE"
	$(AT)build/venv/bin/python3 valenmunge/driver.py gj/earth-coastlines-1m.geo.json
	$(AT)$(NODE) valenmunge/conmunge.js
	$(AT)$(NODE) valenmunge/linkmunge.js

build/.pipstamp: requirements.txt
	$(AT)if [ ! -d build/venv ]; then \
		python3 -m venv build/venv; \
	fi
	$(AT)CC=$(CC) CXX=$(CXX) build/venv/bin/pip3 install -r $< && touch $@

# vi: sw=4 ts=4 noet cc=80
