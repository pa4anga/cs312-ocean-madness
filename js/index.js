// this doesn't embed a licence so we just slap this in
/*! mapbox-gl, see https://github.com/mapbox/mapbox-gl-js/blob/v1.5.1/LICENSE.txt */
const mapbox = require("mapbox-gl")

var map = new mapbox.Map({
	container: "map",
	center: [0, 0],
	zoom: 1,
	style: {
		version: 8,
		name: "Ocean Madness",
		sources: {
			"base": {
				type: "vector",
				url: "/services/base"
			},
			"cities": {
				type: "vector",
				url: "/services/cities"
			}
		},
		layers: [
			{
				id: "bg",
				type: "background",
				paint: { "background-color": "#B8F1FF" }
			},
			{
				id: "base-fill",
				type: "fill",
				source: "base",
				"source-layer": "ashmsworldmapgeo",
				paint: { "fill-color": "#FAFAF7" },
			},
			{
				id: "base-line",
				type: "line",
				source: "base",
				"source-layer": "ashmsworldmapgeo",
				paint: {
					"line-color": "#0066CC",
					"line-width": 1,
				}
			},
			/*{ // this looks kinda bad
				id: "cities-fill",
				type: "fill",
				source: "cities",
				"source-layer": "drei01citiesgeo",
				paint: { "fill-color": "#0066CC" },
			},*/
			{
				id: "cities-names",
				type: "symbol",
				source: "cities",
				"source-layer": "drei01citiesgeo",
				layout: {
					"text-field": ["format", ["get", "NAME"], {}],
					"text-font": ["Open Sans Regular"],
					"text-size": 8
				},
				minzoom: 5
			},
			{
				id: "base-names",
				type: "symbol",
				source: "base",
				"source-layer": "ashmsworldmapgeo",
				layout: {
					"text-field": ["format", ["get", "name"], {}],
					"text-font": ["Open Sans Semibold"],
					"text-size": 12
				},
				minzoom: 2
			}
		],
		glyphs: "https://fonts.openmaptiles.org/{fontstack}/{range}.pbf"
	}
})

map.addControl(new mapbox.NavigationControl());
map.addControl(new mapbox.FullscreenControl());

// vi: sw=4 ts=4 noet cc=80
