#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <err.h>

/*
 * Apparently just running 2 things at once under a shell script is weirdly
 * difficult (if you want them to be killable with ctrl-c), so I just wrote a
 * little C program to do it lol. Argvs are separated by a blank argument.
 */

int main(int argc, char *argv[]) {
	if (argc < 3) return 1;
	++argv;
	while (*argv) {
		char **endargv = argv + 1;
		for (; *endargv && **endargv; ++endargv);
		if (*endargv) *endargv++ = 0;
		pid_t p = fork();
		if (p == -1) err(200, "couldn't fork");
		if (!p) {
			execvp(*argv, argv);
			err(200, "couldn't exec");
		}
		argv = endargv;
	}
	while (wait(0) != -1);
	// who cares about exit codes? (we're just running the test servers)
	return 0;
}
