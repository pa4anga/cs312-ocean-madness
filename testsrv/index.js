const cheerio = require("cheerio")
const express = require("express")
const http = require("http")
const interceptor = require("express-interceptor")
const proxy = require("http-proxy-middleware")
const socket_io = require("socket.io")

const app = express()
const server = http.Server(app)
const io = socket_io(server)

app.use("/", interceptor((req, res) => ({
	isInterceptable: () => /text\/html/.test(res.get('Content-Type')),
	intercept: (body, send) => {
		var $ = cheerio.load(body)
		$('body').append(
			'<script src="/socket.io/socket.io.js"></script>' +
			'<script src="/livereload.js"></script>')
		send($.html())
	}
})))
app.use(express.static("build/pub/"))

app.use("/.reload/", (req, res) => {
	io.emit("reload")
	res.end()
})
app.use("/livereload.js", (req, res) => {
	res.sendFile(__dirname + "/livereload.js")
})

if (process.argv[2] === "--proxy-tilesrv") {
	app.use("/", proxy({target: "http://localhost:8090/", logLevel: "silent"}))
}

server.listen(8080, () => {
	console.log("Running on http://localhost:8080/")
	if (process.platform !== "win32") {
		console.log("(running `./make` will live-reload)")
	}
	else {
		console.log("(running `winmake.bat` will live-reload)")
	}
})

// vi: sw=4 ts=4 noet cc=80
