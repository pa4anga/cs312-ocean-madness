/* This file is dedicated to the public domain. */

#ifndef INC_INTDEFS_H
#define INC_INTDEFS_H

typedef unsigned int uint;
typedef unsigned long ulong;
typedef long long vlong;
typedef unsigned long long uvlong;

typedef signed char s8;
typedef unsigned char u8;
typedef short s16;
typedef unsigned short u16;
typedef int s32; // NOTE: assuming int is 32-bit everywhere that matters
typedef unsigned int u32;
typedef long long s64;
typedef unsigned long long u64;

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
