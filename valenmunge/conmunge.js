var concaveman = require("concaveman")
var fs = require("fs")

console.log("conmunge: applying concave hull")

fs.mkdirSync("build/int/polygons", {recursive: true})
var points = "build/int/points"
fs.readdirSync(points).forEach(f => {
	if (!fs.statSync(points + "/" + f).isDirectory()) return; // weird...
	fs.mkdirSync("build/int/polygons/" + f, {recursive: true})
	fs.readdirSync(points + "/" + f).forEach(f2 => {
		var filename = f + "/" + f2
		var ps = JSON.parse(fs.readFileSync(points + "/" + filename))
		var conc = concaveman(ps, 2)
		fs.writeFileSync("build/int/polygons/" + filename, JSON.stringify(conc))
	})
});
