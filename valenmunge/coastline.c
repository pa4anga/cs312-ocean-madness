#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "vec.h"

struct point { double lon, lat; };
struct pointlist VEC(struct point);

void pointlist_oom(void) {
	// XXX could/should use py exceptions once I can be bothered learning how
	fputs("pointlist OOM, aborting", stderr);
	abort();
}

void pointlist_push(struct pointlist *l, struct point p) {
	if (!vec_push(l, p)) pointlist_oom();
}

struct point pointlist_pop(struct pointlist *l) {
	return vec_pop(l);
}

void pointlist_pushall(struct pointlist *l, struct point *xs, ulong n) {
	if (l->sz + n > l->max && !_vec_make_room(l, sizeof(struct point),
			l->max + n)) {
		pointlist_oom();
	}
	// XXX don't like the multiply here
	memcpy(l->data + l->sz, xs, n * sizeof(struct point));
	l->sz += n;
}
