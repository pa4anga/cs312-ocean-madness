import math
import os
import shutil
import sys
import traceback
import tarfile
from urllib import request
import sys

# XXX factor this into a util file??
def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs, flush=True)

name_fmt = "EarthEnv-DEM90_{:s}"
url_fmt = "http://mirrors.iplantcollaborative.org/earthenv_dem_data/" + \
		"EarthEnv-DEM90/{:s}.tar.gz"

def _tile_name_pattern(long, lat):
	if lat >= 0:
		ns = "N{:02d}".format(lat)
	else:
		ns = "S{:02d}".format(-lat)
	if long >= 0:
		ew = "E{:03d}".format(long)
	else:
		ew = "W{:03d}".format(-long)
	return ns + ew

def _tile_name(long, lat): return name_fmt.format(_tile_name_pattern(long, lat))

def _dl_tile(name):
	try:
		url = url_fmt.format(name)
		eprint("heightcache: download " + name + ".tar.gz", end="")
		with open("build/cache/TMP-heightcache.tar.gz", "w+b") as f:
			with request.urlopen(url) as h:
				shutil.copyfileobj(h, f)
			f.seek(0)
			with tarfile.open(fileobj = f) as t:
				eprint("\rheightcache: extract " + name + ".tar.gz \b", end="")
				t.extract(name + ".bil", "build/cache")
				eprint(" ✓")
				# if we ever need header info:
				#t.extract(name + ".hdr", "build/cache")
		os.remove("build/cache/TMP-heightcache.tar.gz")
	except Exception as e:
		# since we get run under Cython we can't throw/propagate this up
		traceback.print_exc()
		sys.exit(100)

# XXX I'd rather this return open files but this current fits the rest of the
# code - maybe refactor later, possibly, maybe
# for now, returns an extensionless path (apparently the names have to match
# anyway, or... something)
def tile_files(long, lat):
	name = _tile_name(long, lat)
	# *assuming* one file implies the other......
	if not os.path.exists("build/cache/" + name + ".bil"):
		_dl_tile(name)
	return "build/cache/" + name

# vi: sw=4 ts=4 noet cc=80
