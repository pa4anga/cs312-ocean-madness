import os
import struct

import numpy as np

"""
Just hardcoding header values since all the hdr files are basically the same...
here's a sample for reference:

BYTEORDER      I
LAYOUT         BIL
NROWS          6000
NCOLS          6000
NBANDS         1
NBITS          16
BANDROWBYTES   12000
TOTALROWBYTES  12000
PIXELTYPE      SIGNEDINT
ULXMAP         5.00041666666667
ULYMAP         -15.0004166666667
XDIM           0.000833333333333
YDIM           0.000833333333333
NODATA         -32768
"""

FILE_SZ = 6000
TILE_SZ = 0.000833333333333
TILE_HALF = 0.0004166666667

class BilData(object):
	def __init__(self, basepath, is_top):
		self.tileWidth = TILE_SZ
		self.tileHeight = TILE_SZ
		self.pxWidth = FILE_SZ
		self.pxHeight = FILE_SZ
		if is_top:
			self.pxHeight = 3600 # dumb weird special case :(
		self.tileHalf = TILE_HALF
		self.values = np.memmap(basepath + ".bil", dtype = "int16", mode = "r",
				shape = (self.pxHeight, self.pxWidth))

# vi: sw=4 ts=4 noet cc=80
