import signal
import sys
sys.path.append("build/int/cython")

import coastline as c

if __name__ == "__main__":
	# Don't let python throw a KeyboardInterrupt or cython will just eat it
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	file = "earth-coastlines-50m.geo.json"
	if len(sys.argv) == 2:
		file = sys.argv[1]
	processor = c.CoastlineGenerator(0)
	processor.run(file)
