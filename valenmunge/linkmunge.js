var concaveman = require("concaveman")
var fs = require("fs")

console.log("linkmunge: joining everything up!")

var thepolys = []

fs.mkdirSync("build/int/polygons", {recursive: true})
var polygons = "build/int/polygons"
fs.readdirSync(polygons).forEach(f => {
	if (!fs.statSync(polygons + "/" + f).isDirectory()) return; // weird...
	fs.readdirSync(polygons + "/" + f).forEach(f2 => {
		thepolys.push(JSON.parse(fs.readFileSync(polygons + "/" + f + "/" + f2)))
	})
});

var gj = {"type": "Feature", "properties": {"name": "munge"}, "geometry":
{"type":"GeometryCollection", "geometries": [{"type": "MultiPolygon",
"coordinates": thepolys}]}}

fs.writeFileSync("build/munge2m.geo.json", JSON.stringify(gj))
