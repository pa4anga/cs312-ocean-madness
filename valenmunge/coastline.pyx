# cython: boundscheck=False, wraparound=False, nonecheck = False

import bil
from elevation_store import ElevationRegistry
import errno
import geojson
import os
from matplotlib import path as mpltPath
import sys

cdef extern nogil:
	struct PointList:
		unsigned long long sz
		unsigned long long max
		(double, double) *data
	void pointlist_push(PointList *, (double, double))
	void pointlist_pushall(PointList *, (double, double) *, unsigned long long)
	(double, double) pointlist_pop(PointList *)

cdef extern from * nogil:
	# DUMB: calling C via Cython libc wrapper goes through a bunch of Python
	# abstraction for no reason :angry:
	# so instead we just declare these to make Cython codegen happy :)
	void free(void *)
	void *stderr
	int fputs(char *c, void *f)
	int fflush(void *f)

cdef void eprint(char *s) nogil:
	fputs(s, stderr)

def pyeprint(s):
	print(s, file=sys.stderr, flush=True, end="")

# XXX repeating bil.py for speed - not very pretty :(
# also, can't use const for dumb cython reasons :( although compiler will
# probably optimise anyway, I guess
cdef double TILE_SZ = 0.000833333333333;

def loadBaseShoreline(filePath):
	with open(filePath) as f:
		gj = geojson.load(f)
		return gj

def writePoints(ps, filename, tmpname):
	try:
		with open(tmpname, "w") as f:
			# atomic update, don't wanna corrupt if the process is killed
			f.write(str(ps))
			f.flush()
			os.fsync(f.fileno())
		os.rename(tmpname, filename)
	except Exception as e: # let's pray this doesn't happen haHaaAAAa
		try:
			os.unlink(tmpname)
		except:
			pass
		pyeprint("coastline: WARNING: couldn't save " + filename + ": " + str(e))

cdef char[4] transition_locs
transition_locs = [6, 0, 2, 4]
cdef int transitionSea(int dir):
	return transition_locs[dir >> 1]

# more duplication :(
# most of the other code should eventually be moved into here completely
# anyway
cdef double wrapLong(double lon):
	if lon >= 180:
		lon -= 360
	elif lon < -180:
		lon += 360
	return lon

cdef class CoastlineGenerator:
	cdef int deltaWaterLevel
	cdef object elevationRegistry
	cdef (double, double)[8] surroundingPtBuf
	cdef (double, double)[8] surroundingPtBuf2
	cdef PointList workStack, tempLand, tempNonLand
	cdef object seaCoastalPoints
	cdef object landCoastalPoints
	cdef object checkedPoints

	def __init__(self, deltaWaterLevel):
		self.deltaWaterLevel = deltaWaterLevel
		self.elevationRegistry = ElevationRegistry()
		self.workStack.sz = 0
		self.workStack.max = 0
		self.workStack.data = NULL
		self.tempLand.sz = 0
		self.tempLand.max = 0
		self.tempLand.data = NULL
		self.tempNonLand.sz = 0
		self.tempNonLand.max = 0
		self.tempNonLand.data = NULL
		self.seaCoastalPoints = set()
		self.landCoastalPoints = set()
		self.checkedPoints = set()

	def __dealloc__(self):
		free(self.workStack.data)
		free(self.tempLand.data)
		free(self.tempNonLand.data)

	cdef int getElevation(self, double lon, double lat):
		return self.elevationRegistry.getElevation(lon, lat)

	cdef (double, double) snapPoint(self, double lon, double lat):
		return self.elevationRegistry.getSnappedCoord(lon, lat)

	cdef void genSurroundingPoints(self, (double, double) *buf,
			double lon, double lat):
		buf[0] = self.snapPoint(lon, lat + TILE_SZ)
		buf[1] = self.snapPoint(wrapLong(lon + TILE_SZ), lat + TILE_SZ)
		buf[2] = self.snapPoint(wrapLong(lon + TILE_SZ), lat)
		buf[3] = self.snapPoint(wrapLong(lon + TILE_SZ), lat - TILE_SZ)
		buf[4] = self.snapPoint(lon, lat - TILE_SZ)
		buf[5] = self.snapPoint(wrapLong(lon - TILE_SZ), lat - TILE_SZ)
		buf[6] = self.snapPoint(wrapLong(lon - TILE_SZ), lat)
		buf[7] = self.snapPoint(wrapLong(lon - TILE_SZ), lat + TILE_SZ)


	cdef detectLandmasses(self, poly):
		cdef int pElevation
		cdef double lon, lat, plong, plat
		cdef (double, double) p # iterator for self.checkedPoints to avoid python tuples
		cdef int coast # XXX should be "bool" but cython doesn't like that

		operatingArea = mpltPath.Path(poly) # TODO use C/C++ mpl api??

		for lon, lat in poly:
			pointLong, pointLat = self.snapPoint(lon, lat)

			self.genSurroundingPoints(self.surroundingPtBuf, pointLong, pointLat)
			pointlist_pushall(&(self.workStack), self.surroundingPtBuf, 8)

		eprint("coastline:     detecting coastline")
		while self.workStack.sz:
			p = pointlist_pop(&(self.workStack))
			lon, lat = p

			if (lon, lat) in self.checkedPoints:
				continue

			pElevation = self.getElevation(lon, lat)

			# NOTE to future self: this should only ever encounter land inside
			# the workStack if its coming from a land point outside the polygon
			if pElevation - self.deltaWaterLevel > 0:
				if not operatingArea.contains_point((lon, lat)):
					# detected land outside of polygon area
					coast = False
					self.tempLand.sz = 0

					self.genSurroundingPoints(self.surroundingPtBuf, lon, lat)
					# have to iterate on p _then_ destructure to enforce types
					# or cython doesn't optimise well
					for p in self.surroundingPtBuf:
						if p in self.checkedPoints:
							continue
						plong, plat = p

						pElevation = self.getElevation(plong, plat)
						if pElevation - self.deltaWaterLevel > 0:
							pointlist_push(&self.tempLand, p) # land

						else:
							coast = True  # sea
							self.seaCoastalPoints.add(p)
							self.checkedPoints.add(p)

					if coast:
						self.landCoastalPoints.add((lon, lat))
						pointlist_pushall(&self.workStack, self.tempLand.data,
								self.tempLand.sz)

					self.checkedPoints.add((lon, lat))

				else:  # detected land inside of polygon area
					coast = False

					self.genSurroundingPoints(self.surroundingPtBuf, lon, lat)
					for p in self.surroundingPtBuf:
						if p in self.checkedPoints:
							continue
						plong, plat = p

						pElevation = self.getElevation(plong, plat)

						if pElevation - self.deltaWaterLevel > 0:
							# land
							self.genSurroundingPoints(self.surroundingPtBuf2,
									lon, lat)
							for pp in self.surroundingPtBuf2:
								pplong, pplat = p
								if not self.getElevation(pplong, pplat) > 0:
									# "confirmed to be coast"
									# I don't understand - michael
									pointlist_push(&self.workStack, p)
									break
						else:
							coast = True  # sea
							self.seaCoastalPoints.add(p)
							self.checkedPoints.add(p)

					if coast:
						self.landCoastalPoints.add((lon, lat))

					self.checkedPoints.add((lon, lat))

			else:
				if not operatingArea.contains_point((lon, lat)):
					coast = False
					self.tempNonLand.sz = 0

					self.genSurroundingPoints(self.surroundingPtBuf, lon, lat)
					for p in self.surroundingPtBuf:
						if p in self.checkedPoints:
							continue
						plong, plat = p

						pElevation = self.getElevation(plong, plat)
						if pElevation - self.deltaWaterLevel > 0:
							coast = True  # land
							self.landCoastalPoints.add(p)
							self.checkedPoints.add(p)
						else:
							pointlist_push(&self.tempNonLand, p) # sea

					if coast:
						self.seaCoastalPoints.add((lon, lat))
						pointlist_pushall(&self.workStack, self.tempNonLand.data,
								self.tempNonLand.sz)

					self.checkedPoints.add((lon, lat))

				else:  # detected sea inside of polygon area
					coast = False

					# hackish dataset trick to make it be faster
					if lat > 83.1 or lat < -55.1:
						self.checkedPoints.add((lon, lat))

					self.genSurroundingPoints(self.surroundingPtBuf, lon, lat)
					for p in self.surroundingPtBuf:
						if p in self.checkedPoints:
							continue
						plong, plat = p

						pElevation = self.getElevation(plong, plat)

						if pElevation - self.deltaWaterLevel > 0:
							coast = True  # land
							self.landCoastalPoints.add(p)
							self.checkedPoints.add(p)
						else:
							pointlist_push(&self.workStack, p)

					if coast:
						self.seaCoastalPoints.add((lon, lat))

					self.checkedPoints.add((lon, lat))

		ptlist = []
		for p in self.landCoastalPoints:
			ptlist += [[p[0], p[1]]]

		# try to reuse the same memory later
		self.seaCoastalPoints.clear()
		self.landCoastalPoints.clear()
		self.checkedPoints.clear()
		self.workStack.sz = 0

		return ptlist

	def run(self, file):
		processor = CoastlineGenerator(self.deltaWaterLevel)
		eprint("coastline: loading geojson")
		gj = loadBaseShoreline(file)
		eprint(" \xE2\x9C\x93\n") # ✓ - cython doesn't like utf8 >:(
		cdef int n = 1
		total = 0
		dropped = 0
		for mp in gj.geometry[0]['coordinates']:
			total += len(mp)
		for mp in gj.geometry[0]['coordinates']:
			for poly in mp:
				if n % 2000 == 0:
					# XXX MASSIVE BUG: MEMORY LEAK!!!
					# This is the first thing I want to properly fix but we
					# have to rush out the demo so just throw everything out
					# every so often!!!!!! AAAAAA!!!!
					processor = CoastlineGenerator(self.deltaWaterLevel)
				dirnum = n // 100
				filenum = n % 100
				# TODO should dir be hardcoded? meh fine for now
				dirname = "build/int/points/" + str(dirnum)
				filename = dirname + "/" + str(filenum) + ".json"
				if os.path.isfile(filename):
					pyeprint("coastline: point list " + str(filename) +
							" already exists, skipping \n")
					n += 1
					continue
				tmpname = filename + "~~~~"
				if n > 12000:
					pyeprint("coastline: stopping at 12k, just testing!\n")
					return # XXX that'll do FOR NOW! Do the rest soon!!!
				# XXX discarding slow cases FOR NOW! Run the full munge when we
				# can!!!
				if n in { 262, 6704, 6705, 17891 }:
					pyeprint("coastline: WARNING: skipping polygon " + str(n) +
							", it's too slow to get done on time!\n")
					n += 1
					continue
				pyeprint("coastline: processing polygon {} of {}\n".format(n, total))
				#pyeprint(str(poly) + "\n")
				p = processor.detectLandmasses(poly)

				if p == []:
					dropped += 1
					pyeprint("coastline: warning: dropping empty output (" +
							str(dropped) + ")\n")
				else:
					# mkdir -p
					try:
						os.makedirs(dirname)
					except OSError as e:
						if e.errno == errno.EEXIST and os.path.isdir(dirname):
							pass
						else:
							raise
					writePoints(p, filename, tmpname)
				n = n + 1

# vi: ft=python
