/* This file is dedicated to the public domain. */

#ifndef INC_VEC_H
#define INC_VEC_H

#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>

#include "intdefs.h"

/* Vector type from cbits, modified to use 64-bit sizes, a larger starting
 * size of 256 items, and resizing by 1.5x instead of 2 to save memory. */

struct _vec {
	uvlong sz;
	uvlong max;
	void *data;
};

/*
 * Usage: struct VEC(my_type) myvec = {0};
 * Or: struct myvec VEC(my_type);
 * Or: typedef VEC(my_type) myvec;
 */
#define VEC(type) { \
	uvlong sz; \
	uvlong max; \
	type *data; \
}

#ifdef __GNUC__
__attribute__((unused)) // heck off gcc
#endif
static bool _vec_make_room(struct _vec *v, uvlong tsize, uvlong newcnt) {
	if (newcnt < 256) {
		newcnt = 256;
	}
	else {
		/*--newcnt;
		newcnt |= newcnt >> 1; newcnt |= newcnt >>  2; newcnt |= newcnt >>  4;
		newcnt |= newcnt >> 8; newcnt |= newcnt >> 16; newcnt |= newcnt >> 32;
		newcnt++;*/
		// XXX the below is less efficient, but dunno if there's a cleverer way
		// for powers of 1.5
		uvlong x = v->max;
		do x += x >> 1; while (newcnt > x);
		newcnt = x;
	}

	void *new = reallocarray(v->data, newcnt, tsize);
	if (new) {
		v->data = new;
		v->max = newcnt;
	}
	return !!new;
}

#define vec_push(v, val) ( \
	((v)->sz < (v)->max || \
	_vec_make_room((struct _vec *)(v), sizeof(val), (v)->sz + 1)) && \
	((v)->data[(v)->sz++] = (val), true) \
)

#define vec_pop(v) ((v)->data[--(v)->sz])

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
