import math

import numpy

import heightcache
import bil

def convertTileLatToIndex(tileLat):
	return int(math.floor(-tileLat / 5) + 16)

def convertTileLongToIndex(tileLong):
	return int(math.floor(tileLong / 5) + 36)

def convertLatToIndex(lat, pxHeight):
	return int(pxHeight - 1 - math.floor(lat * 1200))

def convertLongToIndex(long):
	return int(math.floor(long * 1200))

def getElevationAtPoint(bil, deltaLong, deltaLat):
	return bil.values[convertLatToIndex(deltaLat, bil.pxHeight),
			convertLongToIndex(deltaLong)]

def wrapLong(long):
	# xd another hack
	if long >= 180:
		long -= 360
	elif long < -180:
		long += 360
	return long

def snapCordToGridCentered(data, long, lat):
	tileLong = math.floor(long / 5) * 5
	tileLat = math.floor(lat / 5) * 5
	snapLat = tileLat + ((-convertLatToIndex(lat - tileLat, 1)) * bil.TILE_SZ \
			+ bil.TILE_HALF)
	snapLong = tileLong + (convertLongToIndex(long - tileLong) * bil.TILE_SZ \
			+ bil.TILE_HALF)
	return round(snapLong, 9), round(snapLat, 9)

class ElevationRegistry:
	def __init__(self):
		self.loadedTiles = dict()
	
	def _getData(self, tileLong, tileLat):
		# this shouldn't be strictly needed but ehh just in case
		tileLong = wrapLong(tileLong)
		if not (tileLong, tileLat) in self.loadedTiles.keys():
			self.loadElevationFile(tileLong, tileLat)

		return self.loadedTiles[(tileLong, tileLat)]

	def getElevation(self, long, lat):
		if lat > 83 or lat < -55:
			# out of current dataset shouldn't be needed but just in case
			return 0

		tileLong = math.floor(long / 5) * 5
		tileLat = math.floor(lat / 5) * 5
		data = self._getData(tileLong, tileLat)
		#long, lat = snapCordToGridCentered(data, long, lat)

		return getElevationAtPoint(data, long - tileLong, lat - tileLat)
	
	# public wrapper
	def getSnappedCoord(self, long, lat):
		tileLong = math.floor(long / 5) * 5
		tileLat = math.floor(lat / 5) * 5
		data = self._getData(tileLong, tileLat)
		return snapCordToGridCentered(data, long, lat)

	def loadElevationFile(self, tileLong, tileLat):
		is_top = False
		if (tileLat >= 80):
			is_top = True
		data = bil.BilData(heightcache.tile_files(tileLong, tileLat), is_top)
		self.loadedTiles[(tileLong, tileLat)] = data
