module ocean-madness

go 1.13

require (
	github.com/labstack/echo/v4 v4.1.11
	github.com/mattn/go-sqlite3 v1.10.0
)
