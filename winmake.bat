@echo off

if not exist build-config.json (
	echo creating default build-config.json - tweak this if something breaks
	copy .build-config.def.json build-config.json > nul
)

:: setup Make and Node
if not exist build\ (
	echo -- first time setup, just a moment
	md build
)
pushd build
if not exist 7za.exe curl -sL https://github.com/develar/7zip-bin/raw/master/win/x64/7za.exe > 7za.exe
if not exist make.exe curl -s ftp://anonymous:anon%%40@ftp.equation.com/make/64/make.exe > make.exe
if not exist node-12.13.0/ (
	curl -s https://nodejs.org/dist/v12.13.0/node-v12.13.0-win-x64.zip > node-12.13.0.zip
	7za.exe x node-12.13.0.zip -bsp0 -bso0
	move node-v12.13.0-win-x64 node-12.13.0 > nul
	del node-12.13.0.zip
)
popd

if not exist build\int\ md build\int
if not exist build\pub\ md build\pub
build\node-12.13.0\node.exe scripts/genmkrules > build\rules.mk
build\make.exe -j "%NUMBER_OF_PROCESSORS%" _RAN_VIA_SCRIPT=1 SHELL=cmd.exe ^
NODE=build\node-12.13.0\node.exe ^
NPM="build\node-12.13.0\npm.cmd --scripts-prepend-node-path=true" %*

:: vi: sw=4 ts=4 noet cc=80
