#!/bin/sh

if [ ! -f build-config.json ]; then
	echo creating default build-config.json - tweak this if something breaks
	cp .build-config.def.json build-config.json
fi

j=`getconf _NPROCESSORS_ONLN 2>/dev/null || sysctl -n hw.ncpu`
mkdir -p build/host build/cache build/int/cython build/pub build/tiles
node scripts/genmkrules > build/rules.mk
make -j$j _RAN_VIA_SCRIPT=1 "$@"

# vi: sw=4 ts=4 noet cc=80
